<?php

namespace Drupal\greeting\Controller;

use Drupal\Core\Controller\ControllerBase;

class PanelController extends ControllerBase {

  public function displayPanelPage() {

  	$config = $this->config('greeting.settings');
  	
    return array(
      '#theme' => 'panel_page',
      '#text' => $config->get('page_text'),
    );
  }

}