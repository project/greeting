<?php

namespace Drupal\greeting\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class GreetingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'greeting_form';
  }

/**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $config = $this->config('greeting.settings');

	$form['page_text'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Content of the Greeting page'),
      '#default_value' => $config->get('page_text'),
      '#description' => $this->t('Allows you to define the text of the Greeting page'),
    );

	return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('greeting.settings');

    $config->set('page_text', $form_state->getValue('page_text'));
    
    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'greeting.settings',
    ];
  }

}